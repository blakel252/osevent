# Open Source Event
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

This is an example event that, at some point, I hope to grow into a reality. This repository will hold the resources, including logos, plans, etc. for the event. Everything in the repository is automatically CC-BY-SA. This means that your contributions belong to you, and they must be shared *alike*, or under a license that's compatible with CC-BY-SA.

## Goals
- [ ] Idk make this an actual event

## How this works
Logos are stored under /logos, plans are stored in /plans.md and /planning. Suggestions should be made in Merge Requests, editing or adding the files.

The content of the individual planned parts will be added to the repository after the event so that you should go to the event to experience it. :-)