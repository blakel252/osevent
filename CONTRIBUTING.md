# How do I contribute to the event?
Use a Merge Request to most effectively suggest changes. After all, pretty much the whole thing is in Markdown.

For Markdown files (i.e. the plans), the procedure is as follows:
1. Go to the file in the repository you want to edit.
2. Click the Edit button (might be a pencil icon). If you don't see this, you might not be signed in.
Clicking this should fork the repository, giving you your own version to edit.
3. In the file, make your changes.
//TODO: the rest of this file, SVG procedures, etc.